package main.java;

import javax.swing.*;
import java.awt.*;

public class GameLife extends JFrame {
    public GameLife(){
        setResizable(false);
        setTitle("Game life");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        JPanel board = new Board(this);
        board.setForeground(Color.WHITE);
        add(board);
        pack();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(()->{
            JFrame w = new GameLife();
            w.setVisible(true);
        });
    }


}
