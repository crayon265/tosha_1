package main.java;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Timer;


public class Board extends JPanel implements ActionListener {
    private JFrame life;
    private Timer timer;
    private List<Integer> x = new ArrayList<Integer>();
    private List<Integer> y = new ArrayList<Integer>();
    private Integer[][] matrix = new Integer[150][150];
    private Image whiteCell;
    private int aliveC;
    private int deadC;
    private boolean onPause;
    private int option;
    private int warningMessage;
    private int generationNumber;
    private int randomCells;
    private int counterCell;
    private int gameOver;
    public Board(JFrame life){
        addKeyListener(new TAdapter());
        this.life = life;
        setFocusable(true);
        setBackground(Color.black);
        setPreferredSize(new Dimension(900,950));
        initGame();
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.print(e.getX() / 6 + " ");
                System.out.println(e.getY() / 6);
                super.mouseClicked(e);
                if (onPause == true) {

                    if (e.getY() >= 50){
                        if (matrix[e.getX() / 6][(e.getY()-50) / 6] == 0 && option == 0) {
                            matrix[e.getX() / 6][(e.getY()-50) / 6] = 1;
                        }
                        else if(matrix[e.getX() / 6][(e.getY()-50) / 6 ] == 1 && option == 0){
                            matrix[e.getX() / 6][(e.getY()-50) / 6 ] = 0;
                        }
                    }
                    if(e.getX() > 730 && e.getX() < 900 && e.getY() > 0 && e.getY() < 50){
                        option = (option + 1) % 6;
                    }

                    /*if (e.getX() > 770 && e.getX() < 900 && e.getY() > 0 && e.getY() < 50) {
                        option = 1;
                    }
                    if (e.getX() > 570 && e.getX() < 770 && e.getY() > 0 && e.getY() < 50) {
                        option = 2;
                    }
                    if (e.getX() > 300 && e.getX() < 570 && e.getY() > 0 && e.getY() < 50) {
                        option = 3;
                    }*/
                    if(e.getX() > 0 && e.getX() < 900 && e.getY() > 50 && e.getY() < 950 && option == 1){
                        setGun(e.getX() / 6, (e.getY()-50) / 6);
                    }
                    if(e.getX() > 0 && e.getX() < 900 && e.getY() > 50 && e.getY() < 950 && option == 2){
                        SpawnReflect(e.getX() / 6, (e.getY()-50) / 6);
                    }
                    if(e.getX() > 0 && e.getX() < 900 && e.getY() > 50 && e.getY() < 950 && option == 3){
                        SpawnPentadakatlon(e.getX() / 6, (e.getY()-50) / 6);
                    }
                    if(e.getX() > 0 && e.getX() < 900 && e.getY() > 50 && e.getY() < 950 && option == 4){
                        setGun2(e.getX() / 6, (e.getY()-50) / 6);
                    }
                    if(e.getX() > 0 && e.getX() < 900 && e.getY() > 50 && e.getY() < 950 && option == 5){
                        erase(e.getX() / 6, (e.getY()-50) / 6);
                    }
                }
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(onPause == false){
            check();
        }
        counterCell();
        repaint();

    }
    private void check(){
        List<Integer> x = new ArrayList();
        List<Integer> y = new ArrayList();
        Integer[][] newMatrix = new Integer[150][150];
        generationNumber += 10;

        int c;
        /*for(int i = 0; i < 150; i++){
            for(int j = 0; j < 150; j++) {
                c = 0;
               if(i > 0 && j > 0 && matrix[i-1][j-1] == 1){
                   c++;
               }
               if(i > 0 && matrix[i-1][j] == 1){
                   c++;
               }
               if(i < 149 && matrix[i+1][j] == 1){
                   c++;
               }
               if(i < 149 && j < 149 && matrix[i+1][j+1] == 1){
                   c++;
               }
               if(j > 0 && matrix[i][j-1] == 1){
                   c++;
               }
               if(j < 149 && matrix[i][j+1] == 1){
                   c++;
               }
               if(i > 0 && j < 149 && matrix[i-1][j+1] == 1){
                   c++;
               }
               if(i < 149 && j > 0 && matrix[i+1][j-1] == 1){
                   c++;
               }
               if(matrix[i][j] == 0 && c == 3){
                   newMatrix[i][j] = 1;
               }
               else if(matrix[i][j] == 1 && c < 2 || c > 3){
                   newMatrix[i][j] = 0;
               }
               else {
                   newMatrix[i][j] = matrix[i][j];
               }
            }
        }*/


        for(int i = 0; i < 150; i++){
            for(int j = 0; j < 150; j++) {
                c = 0;
                if(i > 0 && j > 0 && matrix[i-1][j-1] == 1){
                    c++;
                }
                if(i > 0 && matrix[i-1][j] == 1){
                    c++;
                }
                if(i < 149 && matrix[i+1][j] == 1){
                    c++;
                }
                if(i < 149 && j < 149 && matrix[i+1][j+1] == 1){
                    c++;
                }
                if(j > 0 && matrix[i][j-1] == 1){
                    c++;
                }
                if(j < 149 && matrix[i][j+1] == 1){
                    c++;
                }
                if(i > 0 && j < 149 && matrix[i-1][j+1] == 1){
                    c++;
                }
                if(i < 149 && j > 0 && matrix[i+1][j-1] == 1){
                    c++;
                }
                if(i == 0 && matrix[149][j] == 1){
                    c++;
                }
                if(i == 0 && j > 0 && matrix[149][j-1] == 1){
                    c++;
                }
                if(i == 0 && j < 149 && matrix[149][j+1] == 1){
                    c++;
                }
                if(i == 149 && matrix[0][j] == 1){
                    c++;
                }
                if(i == 149 && j > 0 && matrix[0][j-1] == 1){
                    c++;
                }
                if(i == 149 && j < 149 && matrix[0][j+1] == 1){
                    c++;
                }
                if(j == 0 && matrix[i][149] == 1){
                    c++;
                }
                if(j == 0 && i > 0 && matrix[i - 1][149] == 1){
                    c++;
                }
                if(j == 0 && i < 149 && matrix[i + 1][149] == 1){
                    c++;
                }
                if(j == 149 && matrix[i][0] == 1){
                    c++;
                }
                if(j == 149 && i > 0 &&  matrix[i - 1][0] == 1){
                    c++;
                }
                if(j == 149 && i < 149 && matrix[i+1][0] == 1){
                    c++;
                }
                if(matrix[i][j] == 0 && c == 3){
                    newMatrix[i][j] = 1;
                }
                else if(matrix[i][j] == 1 && c < 2 || c > 3){
                    newMatrix[i][j] = 0;
                }
                else {
                    newMatrix[i][j] = matrix[i][j];
                }
            }
        }

        matrix = newMatrix;
    }
    private void counterCell(){
        aliveC = 0;
        deadC = 0;
        for(int i = 0; i < 150; i++) {
            for(int j = 0; j < 150; j++) {
                if(matrix[i][j] == 1){
                    aliveC++;
                }
                else {
                    deadC++;
                }
            }
        }
    }
    public void erase(int x, int y){
        for(int x1 = 0; x1 < 150; x1++){
            for(int y1 = 0; y1 < 150; y1++){
                if((x1 - x)*(x1 - x) + (y1 - y)*(y1 - y) <= 36){
                    matrix[x1][y1] = 0;
                }
            }
        }

    }
    private void randomCells() {
        for (int i = 0; i < 150; i++) {
            for (int j = 0; j < 150; j++) {
                if ((int) (Math.random() * 100) % 10 == 0) {
                    matrix[i][j] = 1;
                }
            }
        }
    }
    /*private void gameOver() {
        int a = 0;


    }*/

    private class TAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            if(key == KeyEvent.VK_ESCAPE){
                life.dispose();
            }
            if(key == KeyEvent.VK_SPACE){
                if(onPause == false){
                    onPause = true;
                }
                else{
                    onPause = false;
                }
            }
            if(key == KeyEvent.VK_ENTER){
                randomCells();
            }
        }

    }
    private void initGame(){
        onPause = false;
        option = 0;
        generationNumber = 0;
        for(int i = 0; i < 150; i++){
            for(int j = 0; j < 150; j++){
                /*if((int) (Math.random() * 100) % 10 == 0) {
                    matrix[i][j] = 1;
               }
                else{
                    matrix[i][j] = 0;
                }*/
                matrix[i][j] = 0;
            }
        }
        ImageIcon iid = new ImageIcon("src/main/resources/white.Cell4.png");
        whiteCell = iid.getImage();
        timer = new Timer(25,this);
        timer.start();
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawLine(0, 50, 1000,50);
        g.drawLine(670, 0, 670,50);
        //g.drawLine(570, 0, 570,50);
        //g.drawLine(300, 0, 300,50);
        Font font = new Font("TimesRoman",Font.BOLD, 26);
        g.setFont(font);
        g.drawString("Generation: " + generationNumber, 30, 35);
        g.drawString("alive: " + aliveC, 280, 35);
        g.drawString("dead: " + deadC, 450, 35);
        if(warningMessage != 0){
            warningMessage -= 25;
            g.drawString("impossible", 400,400);
        }
        if(option == 0){
            g.drawString("reverseCell", 718, 35);
        }
        if(option == 1){
            g.drawString("SetGun", 740, 35);
        }
        if(option == 2){
            g.drawString("SpawnReflect", 708, 35);
        }
        if(option == 3){
            g.drawString("SpawnPentadakat", 675, 35);
        }
        if(option == 4){
            g.drawString("SetGun2", 740, 35);
        }
        if(option == 5){
            g.drawString("Eraser", 740,35);
        }

        //g.drawString("SetGun", 790, 35);
        //g.drawString("S.Reflect", 590, 35);
        //g.drawString("S.Pentadakatlon", 315, 35);
        for(int i = 0; i < 150; i++){
            for(int j = 0; j < 150; j++){
                if(matrix[i][j] == 1) {
                    g.drawImage(whiteCell, i * 6, j * 6 + 50, this);
                }
            }
        }
        if(onPause == true) {
            g.drawString("PAUSE", 150, 200);
        }
        Toolkit.getDefaultToolkit().sync();
    }
    public void setGun2(int x, int y){
        if(y + 35 >= 150 || x - 8 < 0) {
            warningMessage = 1000;
            return;
        }
        matrix[x-3][y] = 1;
        matrix[x-3][y+1] = 1;
        matrix[x-4][y]= 1;
        matrix[x-4][y+1] = 1;

        matrix[x][y+13] = 1;
        matrix[x-1][y+12] = 1;
        matrix[x-1][y+14] = 1;
        matrix[x-2][y+11] = 1;
        matrix[x-3][y+11] = 1;
        matrix[x-4][y+11] = 1;
        matrix[x-5][y+12] = 1;
        matrix[x-6][y+13] = 1;
        matrix[x-5][y+14] = 1;
        matrix[x-4][y+15] = 1;
        matrix[x-3][y+15] = 1;
        matrix[x-2][y+15]= 1;
        matrix[x-4][y+16] = 1;
        matrix[x-3][y+16] = 1;
        matrix[x-2][y+16] = 1;

        matrix[x-4][y+21] = 1;
        matrix[x-5][y+21] = 1;
        matrix[x-6][y+21] = 1;
        matrix[x-6][y+22] = 1;
        matrix[x-6][y+23] = 1;
        matrix[x-6][y+24] = 1;
        matrix[x-7][y+22] = 1;
        matrix[x-7][y+23] = 1;
        matrix[x-7][y+24]= 1;
        matrix[x-7][y+25] = 1;
        matrix[x-8][y+25] = 1;
        matrix[x-5][y+24] = 1;
        matrix[x-4][y+24] = 1;
        matrix[x-3][y+22] = 1;
        matrix[x-3][y+23] = 1;
        matrix[x-3][y+24] = 1;
        matrix[x-3][y+25] = 1;
        matrix[x-2][y+25] = 1;
        matrix[x-4][y+22] = 1;
        matrix[x-4][y+23] = 1;

        matrix[x-3][y+30] = 1;
        matrix[x-4][y+30] = 1;

        matrix[x-5][y+34] = 1;
        matrix[x-5][y+35] = 1;
        matrix[x-6][y+34] = 1;
        matrix[x-6][y+35] = 1;
    }
    public void setGun(int y, int x){
        /*for(int i = x; i >= x-8; i--){
            for(int j = y; j <= y+36;j++){
                matrix[i][j] = 1;
            }
        }*/

        if(y + 35 >= 150 || x - 8 < 0){
            warningMessage = 1000;
            return;
        }
        matrix[y][x-3] = 1;
        matrix[y+1][x-3] = 1;
        matrix[y][x-4] = 1;
        matrix[y+1][x-4] = 1;

        matrix[y+13][x] = 1;
        matrix[y+12][x-1] = 1;
        matrix[y+14][x-1] = 1;
        matrix[y+11][x-2] = 1;
        matrix[y+11][x-3] = 1;
        matrix[y+11][x-4] = 1;
        matrix[y+12][x-5] = 1;
        matrix[y+13][x-6] = 1;
        matrix[y+14][x-5] = 1;
        matrix[y+15][x-4] = 1;
        matrix[y+15][x-3] = 1;
        matrix[y+15][x-2]= 1;
        matrix[y+16][x-4] = 1;
        matrix[y+16][x-3] = 1;
        matrix[y+16][x-2] = 1;

        matrix[y+21][x-4] = 1;
        matrix[y+21][x-5] = 1;
        matrix[y+21][x-6] = 1;
        matrix[y+22][x-6] = 1;
        matrix[y+23][x-6] = 1;
        matrix[y+24][x-6] = 1;
        matrix[y+22][x-7] = 1;
        matrix[y+23][x-7] = 1;
        matrix[y+24][x-7] = 1;
        matrix[y+25][x-7] = 1;
        matrix[y+25][x-8] = 1;
        matrix[y+24][x-5] = 1;
        matrix[y+24][x-4] = 1;
        matrix[y+22][x-3] = 1;
        matrix[y+23][x-3] = 1;
        matrix[y+24][x-3] = 1;
        matrix[y+25][x-3] = 1;
        matrix[y+25][x-2] = 1;
        matrix[y+22][x-4] = 1;
        matrix[y+23][x-4] = 1;

        matrix[y+30][x-3] = 1;
        matrix[y+30][x-4] = 1;

        matrix[y+34][x-5] = 1;
        matrix[y+35][x-5] = 1;
        matrix[y+34][x-6] = 1;
        matrix[y+35][x-6] = 1;
    }
    public void SpawnPentadakatlon(int y, int x) {
        /*for (int i = x; i >= x - 7; i--) {
            for (int j = y; j <= y + 35; j++) {
                matrix[i][j] = 1;
            }
        }*/
        if(y + 34 >= 150 || x - 6 < 0){
            warningMessage = 1000;
            return;
        }
        matrix[y][x-5] = 1;
        matrix[y+1][x-5] = 1;
        matrix[y+2][x-4] = 1;
        matrix[y+2][x-6] = 1;
        matrix[y+3][x-5] = 1;
        matrix[y+4][x-5] = 1;
        matrix[y+5][x-5] = 1;
        matrix[y+6][x-5] = 1;
        matrix[y+7][x-4] = 1;
        matrix[y+7][x-6] = 1;
        matrix[y+8][x-5] = 1;
        matrix[y+9][x-5] = 1;


        matrix[y+16][x-1] = 1;
        matrix[y+17][x-1] = 1;
        matrix[y+17][x-2] = 1;
        matrix[y+18][x-2] = 1;
        matrix[y+16][x-3] = 1;

        matrix[y+25][x-1] = 1;
        matrix[y+26][x-1] = 1;
        matrix[y+27][x] = 1;
        matrix[y+27][x-2] = 1;
        matrix[y+28][x-1] = 1;
        matrix[y+29][x-1] = 1;
        matrix[y+30][x-1] = 1;
        matrix[y+31][x-1] = 1;
        matrix[y+32][x] = 1;
        matrix[y+32][x-2] = 1;
        matrix[y+33][x-1] = 1;
        matrix[y+34][x-1] = 1;
    }
    public void SpawnReflect(int y, int x) {
        /*for (int i = x; i >= x - 2; i--) {
            for (int j = y; j <= y + 9; j++) {
                matrix[i][j] = 1;
            }
        }*/
        if(y + 9 >= 150 || x - 2 < 0){
            warningMessage = 1000;
            return;
        }
        matrix[y][x-1] = 1;
        matrix[y+1][x-1] = 1;
        matrix[y+2][x] = 1;
        matrix[y+2][x-2] = 1;
        matrix[y+3][x-1] = 1;
        matrix[y+4][x-1] = 1;
        matrix[y+5][x-1] = 1;
        matrix[y+6][x-1] = 1;
        matrix[y+7][x] = 1;
        matrix[y+7][x-2] = 1;
        matrix[y+8][x-1] = 1;
        matrix[y+9][x-1] = 1;
    }
}

//залить на битбакет код. - готово
//добавить на пробел паузу. - готово
//по клику мыши делать определённые клетки живыми или мертвыми.- готово
//замкнуть карту на сферу. - замкнули границы - готово
//добавить функцию, которая спавнит ружье в определенном месте.(в левом верхнем углу)
//добавить функию определения конца игры.
//добавить кнопки, которые появляются в миню паузы, которые показывают какую фигуру заспавнить.
//найти фигуру и добавить в спавн.- готово.
//увеличить размер поля. - готово
//поменять цвет клетки на салатовый. - готово.
//добавиgть функцию случайного распределения в выделенном квадрате.
/*1) исправить баги

        2) переработать меню так, чтобы у тебя была одна кнопка - которая меняет режим


        на которой будет написан текущий режим

        3) добавить пропадающее  сообщение о том, что невозможно поставить фигуру, потому что нет места

        4) добавить хаоса - добавить режим, в котором случайные клетки становятся живыми
        
 */